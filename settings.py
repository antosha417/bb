import os

BASE_PATH = os.path.dirname(os.path.realpath(__file__))

IN_CLOUD = os.environ.get('app', None) is not None

if IN_CLOUD:
    COMMUNICATOR = {
        'port': 8080,
        'host': 'communicator'
    }
    DATAGEN = {
        'port': 8080,
        'host': 'datagen'
    }
    DATABASE = {
        'host': 'mysql',
        'user': 'userOVU',
        'password': 'kF5orQNyutSkfGUo',
    }
else:
    COMMUNICATOR = {
        'port': 9000,
        'host': 'localhost'
    }
    DATAGEN = {
        'port': 8000,
        'host': 'localhost'
    }
    DATABASE = {
        'host': '192.168.99.100',
        'user': 'root',
        'password': 'ppp',
    }

DATAGEN_URL = 'http://{}:{}'.format(
    DATAGEN["host"],
    DATAGEN['port'])

if __name__ == '__main__':
    print(BASE_PATH)
