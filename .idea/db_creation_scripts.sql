CREATE DATABASE IF NOT EXISTS `credentials`;
CREATE DATABASE IF NOT EXISTS `stocks`;

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` int NOT NULL AUTO_INCREMENT,
  `role` varchar(20) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT into `roles` (`role`) VALUES('admin'), ('senior trader'), ('trader');

DROP TABLE IF EXISTS `accounts`;
CREATE TABLE IF NOT EXISTS `accounts` (
  `account_id` int NOT NULL AUTO_INCREMENT,
  `login` varchar(45) NOT NULL,
  `password`  varchar(20) DEFAULT NULL,
  `role_id` int NOT NULL,
  PRIMARY KEY (`account_id`),
  FOREIGN KEY (`role_id`) REFERENCES `roles`(`role_id`) 
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT into `accounts` (`login`, `password`, `role_id`) VALUES('root', 'admin', 1);

DROP TABLE IF EXISTS `instruments`;
CREATE TABLE IF NOT EXISTS `instruments` (
	`instrument_id` int NOT NULL AUTO_INCREMENT,
	`name` varchar(45) NOT NULL,
    PRIMARY KEY (`instrument_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO `instruments` (`name`) VALUES
('Astronomica'), ('Borealis'),
('Celestial'),('Deuteronic'),('Eclipse'),
('Floral'),('Galactia'),('Heliosphere'),
('Interstella'),('Jupiter'),('Koronis'),
('Lunatic');


DROP TABLE IF EXISTS `counterparties`;
CREATE TABLE IF NOT EXISTS `counterparties` (
	`counterparty_id` int NOT NULL AUTO_INCREMENT,
	`name` varchar(45) NOT NULL,
    PRIMARY KEY (`counterparty_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO `counterparties` (`name`) VALUES
('Lewis'), ('Selvyn'),('Richard'),
('Lina'),('John'),('Nidia');

DROP TABLE IF EXISTS `deals`;
CREATE TABLE IF NOT EXISTS `deals` (
	`deal_id` int NOT NULL AUTO_INCREMENT,
    `instrument_id` int NOT NULL,
	`cpty_id` int NOT NULL,
	`price` FLOAT8 NOT NULL,
    `type` varchar(2) NOT NULL,
    `quantity` int DEFAULT NULL,
    `date` TIMESTAMP(6) DEFAULT CURRENT_TIMESTAMP(6),
    PRIMARY KEY (`deal_id`),
    FOREIGN KEY (`instrument_id`) REFERENCES `instruments`(`instrument_id`),
    FOREIGN KEY (`cpty_id`) REFERENCES `counterparties`(`counterparty_id`) 
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;






