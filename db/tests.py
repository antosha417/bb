import db
import datetime
import json
from data_gen.deal_algorithms import RandomDealData


def test_something():
    data_base = db.DB()
    print(data_base.get_counterparties())

def test_deal_from_tuple_to_dict():
    deal_dict = db.deal_from_tuple_to_dict(
        [(5, 1, 97.03648827398362, 'S', 4, datetime.datetime(2019, 8, 13, 10, 29, 11, 410350))])
    assert deal_dict == [{'instrument_id': 5, 'cpty_id': 1, 'price': 97.03648827398362,
                          'type': 'S', 'quantity': 4, 'time': '13-Aug-2019 (10:29:11.410350)'}]


def test_deals_from_to_time():
    data = db.deals_from_to_time(5, '2019-08-12 14:20', '2019-08-13 10:34:59.143261')
    print("RESULT test_deals_from_to_time: ")
    print(data)


def test_deals_minutes_time_interval():
    data = db.deals_minutes_time_interval(3, '2019-08-13 11:35', 100)
    print("RESULT test_deals_time_interval: ")
    print(data)


def test_compute_profit_loss_for_instrument():
    data = db.compute_profit_loss_for_instrument(3, 2, '2019-08-13 13:15')
    print("RESULT test_compute_profit_loss_for_instrument: ")
    print(data)

def test_compute_profit_loss():
    data = db.compute_profit_loss(3, '2019-08-13 13:15')
    print("RESULT test_compute_profit_loss: ")
    print(data)


def test_data_push():
    with open("data_sample.txt", "w+") as f:
        data_gen = RandomDealData()
        instrument_list = data_gen.create_instrument_list

        for _ in range(150):
            data = data_gen.create_random_data(instrument_list)
            f.write(f"{data}\n")
            normalized_data = db.normalize(data)
            db.push(normalized_data)
            print(json.dumps(normalized_data))

