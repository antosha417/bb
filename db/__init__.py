import pymysql.cursors
import datetime
import json
from settings import DATABASE


class DB:
    def __init__(self):
        stocks_config = {
            'user': DATABASE['user'],
            'password': DATABASE['password'],
            'host': DATABASE['host'],
            'db': 'stocks',
            # 'charset': 'utf8mb4',
            'cursorclass': pymysql.cursors.DictCursor,
        }
        print(stocks_config)

        self.connection = pymysql.connect(**stocks_config)
        self.instruments = self.get_instruments()
        self.counterparties = self.get_counterparties()

        print(f'instruments: {self.instruments}')

    def check_status(self):
        """returns 200 if everything is ok"""
        with self.connection.cursor() as cursor:
            cursor.execute("SELECT 200 as status;")
            status = cursor.fetchone()
        return status.get('status', None)

    def get_instruments(self):
        return self.get_static_data_to_dict_from_db('instruments', 'instrument_id')

    def get_counterparties(self):
        return self.get_static_data_to_dict_from_db('counterparties', 'counterparty_id')

    def get_static_data_to_dict_from_db(self, table, field):
        with self.connection.cursor() as cursor:
            cursor.execute(f"SELECT {field}, name FROM {table}")
            data = cursor.fetchall()

        res = {}
        for instrument in data:
            res[instrument['name']] = instrument[field]
        return res

    def normalize(self, data):
        data['instrument_id'] = self.instruments[data['instrumentName']]
        data.pop('instrumentName', None)
        data['cpty_id'] = self.counterparties[data['cpty']]
        data.pop('cpty', None)
        return data

    def push(self, deal):
        with self.connection.cursor() as cursor:
            deal_time = datetime.datetime.strptime(
                deal["time"], '%d-%b-%Y (%H:%M:%S.%f)')
            insert_sql = "INSERT INTO `deals` (`instrument_id`, `cpty_id`, `price`, `type`, `quantity`, `date`) " \
                         "VALUES (%s, %s, %s, %s, %s, %s)"
            data = (deal['instrument_id'], deal['cpty_id'],
                    deal["price"], deal["type"], deal["quantity"], deal_time)
            cursor.execute(insert_sql, data)
        self.connection.commit()

    def deals_from_to_time(self, instrument_id, start_time, end_time):
        """returns json with list of deals"""
        with self.connection.cursor() as cursor:
            start_time = datetime.datetime.fromtimestamp(start_time)
            start_time = datetime.datetime.strftime(
                start_time, '%Y-%m-%d %H:%M:%S.%f')
            end_time = datetime.datetime.fromtimestamp(end_time)
            end_time = datetime.datetime.strftime(
                end_time, '%Y-%m-%d %H:%M:%S.%f')
            select_request = "SELECT `instrument_id`, `cpty_id`, `price`, `type`, `quantity`, `date` " \
                "FROM `deals` " \
                "WHERE `instrument_id` = %s " \
                "AND `date` > %s " \
                "AND `date` <= %s "
            data = (instrument_id, start_time, end_time)
            cursor.execute(select_request, data)
            result = cursor.fetchall()
            for res in result:
                res['date'] = datetime.datetime.strftime(
                    res['date'], '%d-%b-%Y (%H:%M:%S.%f)')
        return result

    def deals_minutes_time_interval(self, instrument_id, start_time, interval_value):
        """returns json with list of deals"""
        with self.connection.cursor() as cursor:
            select_request = "SELECT `instrument_id`, `cpty_id`, `price`, `type`, `quantity`, `date` " \
                "FROM `deals`  " \
                "WHERE `instrument_id` = %s  " \
                "AND `date` > DATE_SUB(%s, interval %s minute) " \
                "AND `date` < %s"
            data = (instrument_id, start_time, interval_value, start_time)
            cursor.execute(select_request, data)
            result = cursor.fetchall()
        return json.dumps(result)

    def change_time_format(self, time):
        time = datetime.datetime.fromtimestamp(time)
        time = datetime.datetime.strftime(time, '%Y-%m-%d %H:%M:%S.%f')
        return time

    def compare_passwords(self, login, password):
        """returns booolean"""
        result = False
        with self.connection.cursor() as cursor:
            find_password = "SELECT `password` from `accounts` WHERE `login`= %s "
            cursor.execute(find_password, login)
            db_password = cursor.fetchone()
            if db_password:
                result = (password == db_password.get('password', None))
        return result

    def realised_profit_loss_for_cpty_in_time_interval(self, cpty_id, start_time):
        """returns json with list of deals"""
        with self.connection.cursor() as cursor:
            start_time = self.change_time_format(start_time)
            profit_request = "SELECT `instrument_id`," \
                             " IF(realised = 0, 0, realised * (avrgS - avrgB)) as realised_profit " \
                             "FROM (SELECT `instrument_id`, quantityS, quantityB, " \
                             "IF(quantityS < quantityB, quantityS, quantityB) as realised," \
                             "quantityB -  quantityS as end_position, " \
                             "IF(quantityS = 0, 0, sumS / quantityS) as avrgS, " \
                             "IF(quantityB = 0, 0, sumB / quantityB) as avrgB " \
                             "FROM (SELECT `instrument_id`, SUM(IF(`type` = 'S', " \
                             "`quantity`, 0)) as quantityS, SUM(IF(`type` = 'B', `quantity`, 0)) as quantityB, " \
                             "SUM(IF(`type` = 'S', `price`*`quantity`, 0)) as sumS , " \
                             "SUM(IF(`type` = 'B', `price`*`quantity`, 0)) as sumB, " \
                             "MAX(IF(`type` = 'S',`deal_id`, 0)) as lastS_deal, " \
                             "MAX(IF(`type` = 'B',`deal_id`, 0)) as lastB_deal " \
                             "FROM (SELECT * FROM `deals` WHERE `cpty_id` = %s " \
                             "AND `date` > DATE_SUB(%s, interval 2 minute) " \
                             "AND `date` <= %s) as tb GROUP BY `instrument_id`) as tb2) as final"
            data = (cpty_id, start_time, start_time)
            cursor.execute(profit_request, data)
            result = cursor.fetchall()
        return result

    def effective_profit_loss_for_cpty_in_time_interval(self, cpty_id, start_time):
        """returns json with list of deals"""
        with self.connection.cursor() as cursor:
            start_time = self.change_time_format(start_time)
            profit_request = "SELECT `instrument_id`, " \
                             "IF(realised = 0, 0 + left_positions * last_price, " \
                             "realised * (avrgS - avrgB) + left_positions * last_price) as effective_pl " \
                             "FROM (SELECT `instrument_id`, quantityS, quantityB,IF(quantityS < quantityB, " \
                             "quantityS, quantityB) as realised, IF(quantityS < quantityB, quantityB - quantityS, " \
                             "quantityB - quantityB) as left_positions, " \
                             "IF(quantityS = 0, 0, sumS / quantityS) as avrgS, " \
                             "IF(quantityB = 0, 0, sumB / quantityB) as avrgB, last_deal, " \
                             "last_price FROM (SELECT * FROM (SELECT `instrument_id`, " \
                             "SUM(IF(`type` = 'S', `quantity`, 0)) as quantityS, " \
                             "SUM(IF(`type` = 'B', `quantity`, 0)) as quantityB, " \
                             "SUM(IF(`type` = 'S', `price`*`quantity`, 0)) as sumS ,  " \
                             "SUM(IF(`type` = 'B', `price`*`quantity`, 0)) as sumB, " \
                             "MAX(`deal_id`) as last_deal FROM (SELECT * FROM `deals` " \
                             "WHERE `cpty_id` = %s AND `date` > DATE_SUB(%s, interval 2 minute) " \
                             "AND `date` <= %s) as tb GROUP BY `instrument_id`) " \
                             "as ddd join (SELECT `deal_id`, `price` as last_price FROM `deals`) " \
                             "as ttt ON (ttt.deal_id = ddd.last_deal)) as base) as final"
            data = (cpty_id, start_time, start_time)
            cursor.execute(profit_request, data)
            result = cursor.fetchall()
        return result

    def average_prices_for_cpty_in_time_interval(self, cpty_id, start_time):
        """returns json with list of deals"""
        with self.connection.cursor() as cursor:
            start_time = self.change_time_format(start_time)
            price_request = "SELECT  `instrument_id`, " \
                            "IF(quantityS = 0, 0, sumS / quantityS) as avrgS, " \
                            "IF(quantityB = 0, 0, sumB / quantityB) as avrgB " \
                            "FROM (SELECT `instrument_id`, SUM(IF(`type` = 'S', `quantity`, 0)) " \
                            "as quantityS, SUM(IF(`type` = 'B', `quantity`, 0)) as quantityB, " \
                            "SUM(IF(`type` = 'S', `price`*`quantity`, 0)) as sumS , " \
                            "SUM(IF(`type` = 'B', `price`*`quantity`, 0)) as sumB " \
                            "FROM (SELECT * FROM `deals` WHERE `cpty_id` = %s " \
                            "AND `date` > DATE_SUB(%s, interval 2 minute) AND `date` <= %s) as tb1 " \
                            "GROUP BY `instrument_id`) as tb2"
            data = (cpty_id, start_time, start_time)
            cursor.execute(price_request, data)
            result = cursor.fetchall()
        return result

    def ending_position_for_cpty_in_time_interval(self, cpty_id, start_time):
        """returns json with list of deals"""
        with self.connection.cursor() as cursor:
            start_time = self.change_time_format(start_time)
            price_request = "SELECT `instrument_id`, SUM(IF(`type` = 'B', `quantity`, - `quantity`)) as sumq " \
                            "FROM (SELECT * FROM `deals` WHERE `cpty_id` = %s " \
                            "AND `date` > DATE_SUB(%s, interval 2 minute) " \
                            "AND `date` <= %s) as tb1 GROUP BY `instrument_id`"
            data = (cpty_id, start_time, start_time)
            cursor.execute(price_request, data)
            result = cursor.fetchall()
            for res in result:
                res['sumq'] = int(res['sumq'])
        return result


"""
def update_users(login, password, role):
    db, cursor = get_db_and_cursor(credentials_db_config)
    check_if_exist = "SELECT `login` from `accounts` WHERE `login` = %s "
    cursor.execute(check_if_exist, (login,))
    existing_login = cursor.fetchall()
    if not existing_login:
        find_role_id = "SELECT `role_id` from `roles` WHERE `role` = %s "
        cursor.execute(find_role_id, (role,))
        role_id = cursor.fetchone()[0]
        insert_new_user = "INSERT INTO `accounts` (`login`, `password`, `role_id`) " \
                          "VALUES (%s, %s, %s)"
        insert_data = (login, password, role_id)
        cursor.execute(insert_new_user, insert_data)
    else:
        update_user_password = "UPDATE `accounts` SET `password` = %s WHERE `login` = %s"
        insert_data = (password, login)
        cursor.execute(update_user_password, insert_data)
    close_db_connection(db, cursor, commit=True)

"""
