from settings import DATAGEN, COMMUNICATOR
import os

if __name__ == '__main__':
    app_name = os.environ.get('app', None)
    
    if not app_name:
        print('Define `app` env variable. Set it to `datagen` or `communicator`')
        exit(1)

    if app_name == 'datagen':
        from backend.datagen import app
    elif app_name == 'communicator':
        from backend.communicator import app
    else:
        print('Define `app` env variable. Set it to `datagen` or `communicator`')
        exit(1)

    app.run(port=8080, threaded=True, host='0.0.0.0')
