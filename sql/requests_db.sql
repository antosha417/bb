-- AVERAGE SELL/BUY PRICE

SELECT  `instrument_id`,
IF(quantityS = 0, 0, sumS / quantityS) as avrgS,
IF(quantityB = 0, 0, sumB / quantityB) as avrgB
FROM
	(SELECT `instrument_id`, SUM(IF(`type` = 'S', `quantity`, 0)) as quantityS,
	 SUM(IF(`type` = 'B', `quantity`, 0)) as quantityB,
	 SUM(IF(`type` = 'S', `price`*`quantity`, 0)) as sumS ,  SUM(IF(`type` = 'B', `price`*`quantity`, 0)) as sumB
	FROM
		(SELECT * FROM `deals`
		WHERE `cpty_id` = 3 AND
		`date` > DATE_SUB('2019-08-13 13:15', interval 100 minute)
				AND `date` <= '2019-08-13 13:15') as tb1
	GROUP BY `instrument_id`) as tb2;

-- END POSITION

SELECT  `instrument_id`,
quantityB -  quantityS as end_position
FROM
	(SELECT `instrument_id`, SUM(IF(`type` = 'S', `quantity`, 0)) as quantityS,
	 SUM(IF(`type` = 'B', `quantity`, 0)) as quantityB,
	 SUM(IF(`type` = 'S', `price`*`quantity`, 0)) as sumS ,  SUM(IF(`type` = 'B', `price`*`quantity`, 0)) as sumB
	FROM
		(SELECT * FROM `deals`
		WHERE `cpty_id` = 3 AND
		`date` > DATE_SUB('2019-08-13 13:15', interval 100 minute)
				AND `date` <= '2019-08-13 13:15') as tb1
	GROUP BY `instrument_id`) as tb2;

--  BASE TABLE
    SELECT * FROM
(SELECT `instrument_id`, SUM(IF(`type` = 'S', `quantity`, 0)) as quantityS,
 SUM(IF(`type` = 'B', `quantity`, 0)) as quantityB,
 SUM(IF(`type` = 'S', `price`*`quantity`, 0)) as sumS ,  SUM(IF(`type` = 'B', `price`*`quantity`, 0)) as sumB,
MAX(IF(`type` = 'S',`deal_id`, 0)) as lastS_deal, MAX(IF(`type` = 'B',`deal_id`, 0)) as lastB_deal
FROM
	(SELECT * FROM `deals`
	WHERE `cpty_id` = 3 AND
	`date` > DATE_SUB('2019-08-13 13:15', interval 100 minute)
			AND `date` <= '2019-08-13 13:15') as tb
GROUP BY `instrument_id`) as ddd
join
(select `deal_id`, `price` as lastS_price from `deals`) as ttt
on (ttt.deal_id = ddd.lastS_deal)
join
(select `deal_id`, `price` as lastB_price from `deals`) as rrr
on (rrr.deal_id = ddd.lastB_deal);


--  FINAL BASE

SELECT * FROM
(SELECT `instrument_id`, SUM(IF(`type` = 'S', `quantity`, 0)) as quantityS,
 SUM(IF(`type` = 'B', `quantity`, 0)) as quantityB,
 SUM(IF(`type` = 'S', `price`*`quantity`, 0)) as sumS ,  SUM(IF(`type` = 'B', `price`*`quantity`, 0)) as sumB,
MAX(`deal_id`) as last_deal
FROM
	(SELECT * FROM `deals`
	WHERE `cpty_id` = 3 AND
	`date` > DATE_SUB('2019-08-13 13:15', interval 100 minute)
			AND `date` <= '2019-08-13 13:15') as tb
GROUP BY `instrument_id`) as ddd
join
(select `deal_id`, `price` as last_price from `deals`) as ttt
on (ttt.deal_id = ddd.last_deal);

-- REALISED PROFIT/LOSS

SELECT `instrument_id`, IF(realised = 0, 0, realised * (avrgS - avrgB))  as realised_pl
FROM
	(SELECT  `instrument_id`, quantityS, quantityB,
	IF(quantityS < quantityB, quantityS, quantityB) as realised,
	quantityB -  quantityS as end_position,
	IF(quantityS = 0, 0, sumS / quantityS) as avrgS,
	IF(quantityB = 0, 0, sumB / quantityB) as avrgB
	FROM
		(SELECT `instrument_id`, SUM(IF(`type` = 'S', `quantity`, 0)) as quantityS,
		 SUM(IF(`type` = 'B', `quantity`, 0)) as quantityB,
		 SUM(IF(`type` = 'S', `price`*`quantity`, 0)) as sumS ,  SUM(IF(`type` = 'B', `price`*`quantity`, 0)) as sumB,
		MAX(IF(`type` = 'S',`deal_id`, 0)) as lastS_deal, MAX(IF(`type` = 'B',`deal_id`, 0)) as lastB_deal
		FROM
		(SELECT * FROM `deals`
		WHERE `cpty_id` = 3 AND
		`date` > DATE_SUB('2019-08-13 13:15', interval 100 minute)
				AND `date` <= '2019-08-13 13:15') as tb
		GROUP BY `instrument_id`) as tb2
	) as final;

SELECT `instrument_id`,
 IF(realised = 0, 0 + left_positions * last_price, realised * (avrgS - avrgB) + left_positions * last_price)  as effective_pl
FROM
	(SELECT `instrument_id`, quantityS, quantityB,IF(quantityS < quantityB, quantityS, quantityB) as realised,
IF(quantityS < quantityB, quantityB - quantityS, quantityB - quantityB) as left_positions,
  IF(quantityS = 0, 0, sumS / quantityS) as avrgS,
	IF(quantityB = 0, 0, sumB / quantityB) as avrgB, last_deal, last_price
FROM
	(SELECT * FROM
		(SELECT `instrument_id`, SUM(IF(`type` = 'S', `quantity`, 0)) as quantityS,
		 SUM(IF(`type` = 'B', `quantity`, 0)) as quantityB,
		 SUM(IF(`type` = 'S', `price`*`quantity`, 0)) as sumS ,  SUM(IF(`type` = 'B', `price`*`quantity`, 0)) as sumB,
		MAX(`deal_id`) as last_deal
		FROM
			(SELECT * FROM `deals`
			WHERE `cpty_id` = 3 AND
			`date` > DATE_SUB('2019-08-13 13:15', interval 100 minute)
					AND `date` <= '2019-08-13 13:15') as tb
		GROUP BY `instrument_id`) as ddd
	join
		(SELECT `deal_id`, `price` as last_price FROM `deals`) as ttt
		ON (ttt.deal_id = ddd.last_deal)
    ) as base
    ) as final;


