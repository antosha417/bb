import time
from datetime import datetime, timedelta

import numpy
import random

from data_gen.instrument import Instrument
import os
from settings import BASE_PATH

instruments = ("Astronomica", "Borealis", "Celestial", "Deuteronic", "Eclipse",
               "Floral", "Galactia", "Heliosphere", "Interstella", "Jupiter", "Koronis", "Lunatic")
counterparties = ("Lewis", "Selvyn", "Richard", "Lina", "John", "Nidia")
NUMBER_OF_RANDOM_DEALS = 2000
TIME_PERIOD_MILLIS = 3600000
EPOCH = datetime.now() - timedelta(days=1)


class RandomDealData:
    @property
    def create_instrument_list(self):

        f = open(os.path.join(BASE_PATH, 'data_gen',
                              'initialRandomValues.txt'), 'r')
        instrument_id = 1000
        instrument_list = []

        for instrument_name in instruments:
            hashed_value = int(f.readline())
            is_negative = hashed_value < 0
            base_price = (abs(hashed_value) % 10000) + 90.0
            drift = ((abs(hashed_value) % 5) * base_price) / 1000.0
            drift = 0 - drift if is_negative else drift
            variance = (abs(hashed_value) % 1000) / 100.0
            variance = 0 - variance if is_negative else variance
            instrument = Instrument(
                drift, variance + 100, base_price, variance, instrument_name)
            instrument_list.append(instrument)
            instrument_id += 1
        return instrument_list

    @staticmethod
    def create_random_data(instrument_list):
        time.sleep(random.uniform(1, 30) / 100)
        deal_id = 20000
        instrument = instrument_list[numpy.random.randint(
            0, len(instrument_list))]
        cpty = counterparties[numpy.random.randint(0, len(counterparties))]
        _type = 'B' if numpy.random.choice([True, False]) else 'S'
        quantity = int(numpy.power(1001, numpy.random.random()))
        deal_time = datetime.now() - timedelta(days=1)
        deal_id += 1
        deal = {
            'instrumentName': instrument.name,
            'cpty': cpty,
            'price': instrument.calculate_next_price(_type),
            'type': _type,
            'quantity': quantity,
            'time': deal_time.strftime("%d-%b-%Y (%H:%M:%S.%f)"),
        }
        return deal
