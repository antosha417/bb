from data_gen.deal_algorithms import RandomDealData
import db

def test_data_get():
    with open("data_sample.txt", "w+") as f:
        data_gen = RandomDealData()
        instrument_list = data_gen.create_instrument_list

        for _ in range(15):
            deal = data_gen.create_random_data(instrument_list)
            f.write(f"{deal}\n")
            print(deal)


def test_db_connection():
    data_gen = RandomDealData()
    instrument_list = data_gen.create_instrument_list

    for _ in range(15):
        deal = data_gen.create_random_data(instrument_list)
        db.push(db.normalize(deal))
