import numpy


class Instrument:

    def __init__(self, drift, price, starting_price, variance, name):
        self.__drift = drift
        self.__price = price
        self.__starting_price = starting_price
        self.__variance = variance
        self.name = name

    def calculate_next_price(self, direction):
        new_price_starter = self.__price + numpy.random.normal(0, 1) * self.__variance + self.__drift
        new_price = new_price_starter if (new_price_starter > 0) else 0.0
        if self.__price < self.__starting_price * 0.4:
            self.__drift = (-0.7 * self.__drift)
            self.__price = new_price * 1.01 if direction == 'B' else new_price * 0.99
        return self.__price
