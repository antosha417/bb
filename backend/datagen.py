from flask import Flask, Response, request
from flask_cors import CORS
import json
from data_gen.deal_algorithms import RandomDealData
import db
from settings import DATAGEN
import time

app = Flask(__name__)
CORS(app)

data_gen = RandomDealData()
instrument_list = data_gen.create_instrument_list
DATABASE = db.DB()


@app.route('/ping')
def pong():
    return 'pong'


@app.route('/check_db')
def check_db():
    status = {'status': ['not_ok', 'ok'][DATABASE.check_status() == 200]}
    return Response(json.dumps(status))


@app.route('/streaming_data')
def streaming():
    def event_stream():
        while True:
            time.sleep(0.1)
            data = data_gen.create_random_data(instrument_list)
            normalized_data = DATABASE.normalize(data)
            # DATABASE.push(normalized_data)
            yield f'{json.dumps(normalized_data)}\n'
    return Response(event_stream(), mimetype="text/event-stream")


@app.route('/get_instruments')
def db_get_instruments():
    data = DATABASE.get_instruments()
    return json.dumps(data)


@app.route('/get_cpty')
def db_get_cpty():
    data = DATABASE.get_counterparties()
    return json.dumps(data)


@app.route('/get_data_from_to_time')
def db_deals_from_to_time():
    instrument_id = int(request.args.get('instrument_id'))
    start_time = float(request.args.get('time_start'))
    end_time = float(request.args.get('time_end'))
    # TO DO MISTAKE HANDLER
    data = DATABASE.deals_from_to_time(instrument_id, start_time, end_time)
    return json.dumps(data)


@app.route('/check_password')
def check_password():
    login = request.args.get('login')
    password = request.args.get('password')
    data = DATABASE.compare_passwords(login, password)
    return json.dumps({'success': data})


@app.route('/get_effective_profit_loss')
def get_effective_profit_loss():
    cpty_id = int(request.args.get('cpty_id'))
    start_time = float(request.args.get('time_start'))
    # TO DO MISTAKE HANDLER
    data = DATABASE.effective_profit_loss_for_cpty_in_time_interval(
        cpty_id, start_time)
    return json.dumps(data)


@app.route('/get_realised_profit_loss')
def get_realised_profit_loss():
    cpty_id = int(request.args.get('cpty_id'))
    start_time = float(request.args.get('time_start'))
    # TO DO MISTAKE HANDLER
    data = DATABASE.realised_profit_loss_for_cpty_in_time_interval(
        cpty_id, start_time)
    return json.dumps(data)


@app.route('/get_average_prices')
def get_average_prices():
    cpty_id = int(request.args.get('cpty_id'))
    start_time = float(request.args.get('time_start'))
    # TO DO MISTAKE HANDLER
    data = DATABASE.average_prices_for_cpty_in_time_interval(
        cpty_id, start_time)
    return json.dumps(data)


@app.route('/get_ending_positions')
def get_ending_positions():
    cpty_id = int(request.args.get('cpty_id'))
    start_time = float(request.args.get('time_start'))
    # TO DO MISTAKE HANDLER
    data = DATABASE.ending_position_for_cpty_in_time_interval(
        cpty_id, start_time)
    return json.dumps(data)


if __name__ == '__main__':
    app.run(port=DATAGEN['port'], threaded=True, host='0.0.0.0')
