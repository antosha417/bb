from flask import Flask, Response
import requests
from flask_cors import CORS
import settings
import json

app = Flask(__name__)
CORS(app)


@app.route('/ping')
def pong():
    return 'pong'


@app.route('/db_status')
def db_status():

    response = requests.get(settings.DATAGEN_URL + '/check_db')
    status = {'status': 'not_ok'}
    print(response, response.status_code, response.ok, response.text)
    if response.ok:
        data = json.loads(response.text)
        if data.get('status') == 'ok':
            status['status'] = 'ok'
    return Response(json.dumps(status))


@app.route('/get_data')
def get_data():
    response = requests.get(settings.DATAGEN_URL +'/streaming_data', stream=True)

    def data_stream():
        for data in response.iter_lines(chunk_size=1):
            yield f'data:{data.decode("utf-8")}\n\n'

    return Response(data_stream(), mimetype="text/event-stream")


if __name__ == '__main__':
    app.run(port=settings.COMMUNICATOR['port'], threaded=True, host='0.0.0.0')
